package day1.operatory;

public class OperatoryMatematyczne {
    public static void main(String[] args) {
        int a = 16;
        int b = 10;
        System.out.println("Obliczenia dla liczb. a = " + a + " b = " + b);

        int wynikDodawania = a + b;
        System.out.println("Dodawanie: " + wynikDodawania);

        int wynikOdejmowania = a - b;
        System.out.println("Odejmowanie: " + wynikOdejmowania);

        int wynikMnozenia = a * b;
        System.out.println("Mnozenie: " + wynikMnozenia);

        int wynikDzielenia = a / b;
        System.out.println("Dzielenie:" + wynikDzielenia);

        int wynikModulo = a % b;
        System.out.println("Reszta: " + wynikModulo);

        //Dzielenie int przez int
        System.out.println("-------------------");
        int x = 5;
        int y = 4;
        System.out.println("Dzielenie int przez int: " + x / y);

        // Dzielenie z rzutowaniem
        System.out.println("Dzielenie: " + (float) x / y);
    }
}
