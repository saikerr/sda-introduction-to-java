package day2.instrukcjesterujace;

public class ScopeIntro {
    public static void main(String[] args) {

        //zmienna f zdefiniowana w zasiegu funkcji main - jest widoczna od momentu deklaracji
        // do konca funkcji main
        int f = 0;

        if (f > 10) {
            //zmienna wynik widoczna od momentu deklaracji
            // do konca tego if'a
            int wynik = f * 10;
            System.out.println(wynik);

        }
        if (f < 10) {
            //inny zasieg
            int wynik = f + 20;
            System.out.println(wynik);
        }

    }
}
