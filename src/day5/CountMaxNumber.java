package day5;

import java.util.Random;

public class CountMaxNumber {
    public static void main(String[] args) {
        // Wypelnic losowymi wartosciami
        Random rd = new Random(2);

        //zakres od 1 do 30
        int size = rd.nextInt(30) + 1;
        int[] numbers = new int[size];
        // znalezc max
        for (int i = 0; i< numbers.length; i++){
            numbers[i] = rd.nextInt(20);
        }

        // Znalezenie najwiekszej liczby
        int max = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            if (max < numbers[i]) {
                max = numbers[i];
            }
        }
        //Zliczyc ile razy max sie pojawia
        int count = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (max == numbers[i]) {
                count++;
            }
        }

        ArrayHelper.printArray(numbers);

        System.out.println("Najwieksza liczba to: " + max + " Liczba wystapien: " + count);

    }
}
