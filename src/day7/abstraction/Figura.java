package day7.abstraction;

public abstract class Figura {
    public abstract double obliczPole();

    public abstract double obliczObwod();
}
