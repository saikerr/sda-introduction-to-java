package day7.inheritance;

import day7.interfaces.Payable;
import day7.interfaces.Workable;

public class StudentZaoczny extends Student implements Workable, Payable{
    public StudentZaoczny(String name, String surname, int age) {
        super(name, surname, age);
    }

    @Override
    public void work() {

    }

    @Override
    public double getSalary() {
        return 0;
    }

    @Override
    public void pay() {

    }
}
