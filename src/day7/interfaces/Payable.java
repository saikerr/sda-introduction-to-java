package day7.interfaces;

public interface Payable {
    void pay();
}
