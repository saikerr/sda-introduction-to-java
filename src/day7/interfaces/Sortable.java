package day7.interfaces;

public interface Sortable {
    void sort();
}
